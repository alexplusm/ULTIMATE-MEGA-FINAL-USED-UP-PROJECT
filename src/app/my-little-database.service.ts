import {ITicket} from './core/model/i-ticket';

export class MyLittleDatabase {
  ticket1: ITicket = {
    ticketNumber: 1,
    questions: [
      {
        ticketNumber: 1,
        questionNumber: 1,
        image: '/assets/2.jpg',
        questText: 'БИЛЕТ1-Вы намерены произвести разворот на перекрестке.' +
        ' Какие указатели поворота необходимо включить перед въездом на перекресток?',
        answers: [
          'Правого поворота',
          'Левого поворота',
          'Включать указатели поворота при необходимости',
        ],
        trueAnswer: 'Правого поворота'
      },
      {
        ticketNumber: 1,
        questionNumber: 2,
        image: '/assets/3.jpg',
        questText: 'БИЛЕТ1-На каком рисунке изображен автомобиль, водитель которого нарушает правила перевозки грузов?',
        answers: [
          'Только на Б',
          'Только на А',
          'На обоих',
        ],
        trueAnswer: 'Только на А'
      },
      {
        ticketNumber: 1,
        questionNumber: 3,
        image: '/assets/4.jpg',
        questText: 'БИЛЕТ1-Кому Вы должны уступить дорогу при повороте налево?',
        answers: [
          'Только грузовому автомобилю',
          'Только легковому автомобилю',
          'Обоим транспортным средствам',
        ],
        trueAnswer: 'Обоим транспортным средствам'
      }
    ]
  };
  ticket2: ITicket = {
    ticketNumber: 2,
    questions: [
      {
        ticketNumber: 2,
        questionNumber: 1,
        image: '/assets/2.jpg',
        questText: 'БИЛЕТ2-Вы намерены произвести разворот на перекрестке.' +
        ' Какие указатели поворота необходимо включить перед въездом на перекресток?',
        answers: [
          'Правого поворота',
          'Левого поворота',
          'Включать указатели поворота при необходимости',
        ],
        trueAnswer: 'Правого поворота'
      },
      {
        ticketNumber: 2,
        questionNumber: 2,
        image: '/assets/3.jpg',
        questText: 'БИЛЕТ2-На каком рисунке изображен автомобиль, водитель которого нарушает правила перевозки грузов?',
        answers: [
          'Только на Б',
          'Только на А',
          'На обоих',
        ],
        trueAnswer: 'Только на А'
      },
      {
        ticketNumber: 2,
        questionNumber: 3,
        image: '/assets/4.jpg',
        questText: 'БИЛЕТ2-Кому Вы должны уступить дорогу при повороте налево?',
        answers: [
          'Только грузовому автомобилю',
          'Только легковому автомобилю',
          'Обоим транспортным средствам',
        ],
        trueAnswer: 'Обоим транспортным средствам'
      }
    ]
  };

  tickets: ITicket[] = [this.ticket1, this.ticket2];

  getTicket(ticketNumber: number) {
    return this.tickets[ticketNumber - 1];
  }
}
