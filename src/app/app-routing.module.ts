import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './shared/not-found/not-found.component';

const routes: Routes = [

    // см -> auth-routing
  // { path: '', redirectTo: 'login', pathMatch: 'full' },

  { path: 'core', loadChildren: './core/core.module#CoreModule' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
