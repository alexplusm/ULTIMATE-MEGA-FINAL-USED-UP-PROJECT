import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class NotifyService {

  visible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  notificationText$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  type$: BehaviorSubject<string> = new BehaviorSubject<string>('common');

  constructor() { }
  // types: common, warning, success
  generateNotification(alertText: string, type = 'common', duration = 3500) {
    this.visible$.next(true);
    this.notificationText$.next(alertText);
    this.type$.next(type);

    this.closeNotification(duration);
  }

  closeNotification(duration: number) {
    setTimeout(() => {
      this.visible$.next(false);
    }, duration);
  }

}
