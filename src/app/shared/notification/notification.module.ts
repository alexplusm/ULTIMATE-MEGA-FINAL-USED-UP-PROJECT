import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification.component';
import {NotifyService} from './notify.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NotificationComponent],
  exports:[NotificationComponent],
  providers:[NotifyService]
})
export class NotificationModule { }
