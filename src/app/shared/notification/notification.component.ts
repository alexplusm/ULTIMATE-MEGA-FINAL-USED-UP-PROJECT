import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotifyService} from './notify.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit, OnDestroy {

  visible = false;
  text = 'hello world';
  type: string;

  private subs: Subscription[] = [];

  constructor(private notifyService: NotifyService) { }

  ngOnInit() {
    this.subs.push(this.notifyService.visible$.subscribe(value => this.visible = value));
    this.subs.push(this.notifyService.notificationText$.subscribe(text => this.text = text));
    this.subs.push(this.notifyService.type$.subscribe(type => this.type = type));

  }

  onClick() {
    this.visible = !this.visible;
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
