import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../auth/auth.service';
import {NotifyService} from './notification/notify.service';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private notifyService: NotifyService,
    private authService: AuthService,
    private router: Router) {
  }
  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.getUser$()
      .map((user) => {
        if (user) {
          console.log('Guard-true', user);

          return true;
        }

        console.log('Guard-false', user);

        this.notifyService.generateNotification(
          'Анон, залогинься',
          'warning'
        );
        this.router.navigate(['/login']);
        return false;
      });
  }
}
