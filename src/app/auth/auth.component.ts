import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

import {NotifyService} from '../shared/notification/notify.service';
import {AuthService} from './auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {

  userEmail$: Observable<string>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private notifyService: NotifyService) { }

  ngOnInit() {
    this.userEmail$ = this.authService.getUserEmail$();
  }

  // мб через routerLink?
  toCore() {
    this.router.navigate(['/core']);
  }

  logout() {
    this.authService.logout();
    this.notifyService.generateNotification(
      'Досвидули',
      'success'
    );
  }
}
