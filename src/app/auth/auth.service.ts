import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import {IUser} from '../core/model/i-user';
import {ILogin} from '../core/model/i-login';

@Injectable()
export class AuthService {

  private user$ = new BehaviorSubject<IUser>(null);
  // private loggedInSubject = new BehaviorSubject<boolean>(false);


  constructor(private fireAuth: AngularFireAuth, private router: Router) {

    this.fireAuth.authState
      .subscribe((authState) => {
        console.log('--- authState', authState);
        if (!authState) {
          this.user$.next(null);
          return;
        }

        this.user$.next({
          email: authState.email,
          uid: authState.uid
        });
      });
  }

  getUser$(): Observable<IUser> {
    return this.user$.asObservable();
  }

  getUserId$(): Observable<string> {
    return this.getUser$()
      .map((user) => user ? user.uid : null);
  }

  getUserEmail$(): Observable<string> {
    return this.getUser$()
      .map((user) => user ? user.email : null);
  }

  getUserEmail(): string {
    return this.user$.getValue().email;
  }


  login({email, password}: ILogin): Observable<any> {
    return Observable.fromPromise(
      this.fireAuth
        .auth
        .signInWithEmailAndPassword(email, password)
    );
      // .do(() => {
      //  редирект в login.component
      // });
  }

  signUp({email, password}: ILogin): Observable<any> {
    return Observable.fromPromise(
      this.fireAuth
        .auth
        .createUserWithEmailAndPassword(email, password)
    )
      // борьба с авторизацией сразу после создания аккаунта
      .do(() => this.logout());

    //   .do(() => {
    //      редирект в regist.component
    //   // в случае успеха - редирект на логин
    //   this.router.navigate(['login']);
    // });
  }

  logout() {
    this.fireAuth
      .auth
      .signOut()
      .then(() => this.router.navigate(['/']));
  }




  getErrors(errors: any): string {

    if (errors !== null) {
      if (errors['required']) {
        return 'поле обязательно для заполнения';
      }
      if (errors['email']) {
        return 'Введите действительный адрес электронной почты';
      }
      if (errors['maxlength']) {
        return `максимальная длина — ${errors['maxlength']['requiredLength']}`;
      }
      if (errors['minlength']) {
        return `минимальная длина — ${errors['minlength']['requiredLength']}`;
      }
      if (errors['pattern']) {
        return `нужна маленькая и большая английская буковка,
                циферка и какой нибудь спецсимвол (!#$-+)`;
      }
      if (errors['passConfirmError']) {
        return `Пароли не совпадают`;
      }
    }
    return;
  }

}
