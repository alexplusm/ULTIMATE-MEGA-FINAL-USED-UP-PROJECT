import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AuthComponent } from './auth.component';
import {LoginModule} from './login/login.module';
import {RegistrationModule} from './registration/registration.module';
import {AuthRoutingModule} from './auth-routing.module';
import {AuthService} from './auth.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    LoginModule,
    RegistrationModule,
    AuthRoutingModule,
  ],
  declarations: [AuthComponent],
  exports: [AuthComponent],
  providers: [AuthService]
})
export class AuthModule { }
