import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {AuthService} from '../auth.service';
import {NotifyService} from '../../shared/notification/notify.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  re = /(?=.*[0-9])(?=.*[_!#+\-$])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z_!#+\-$]/g;

  constructor(private authService: AuthService,
              private notifyService: NotifyService,
              private router: Router) {}

  ngOnInit() {

    this.form = new FormGroup({
      email: new FormControl(null,
        [Validators.required,
          Validators.maxLength(100),
          Validators.email]),
      password: new FormGroup({
        passFirst: new FormControl(null,
          [Validators.minLength(5),
            Validators.pattern(this.re),
            Validators.required]),
        passSecond: new FormControl(null)
      }, {validators: this.checkPasswords.bind(this)}),
      checkConfirm: new FormControl(null, Validators.requiredTrue),
    });

  }

  checkPasswords(group: FormGroup) {
    const passFirst = group.value.passFirst;
    const passSecond = group.value.passSecond;
    return (passFirst === passSecond) ? null : { passConfirmError: true };
  }


  getErrors(errors: any) {
    return this.authService.getErrors(errors);
  }

  emailValidation() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }

  passFirstValidation() {
    const b = this.form.get('password.passFirst').invalid
      && this.form.get('password.passFirst').touched;

    return b;
  }

  passSecondValidation() {
    const b = this.form.get('password').invalid
      && this.form.get('password.passSecond').touched
      && this.form.get('password.passFirst').dirty;

    return b;
  }

  onSubmit(pass: HTMLInputElement) {

    this.form.controls.email.markAsTouched();
    this.form.controls.checkConfirm.markAsTouched();

    if (this.form.valid) {

      const password: string = pass.value;
      const email: string = this.form.controls.email.value;

      // узнать про отписку в этом случае и в случае логина!!!!
      this.authService.signUp({email, password})
        .subscribe(value => {
          // alert(`REgistration.component Успех при регистрации - value - ${value}`);
          this.router.navigate(['/login']);
          this.notifyService.generateNotification(
            `Успешная регистрация`,
            'success'
          );

        }, error => {
          // alert(`REgistration.component Ошибка при регистрации - Error - ${error}`);
          this.notifyService.generateNotification(
            `Ошибка при регистрации : ${error}`,
            'warning'
          );

        });

      this.form.reset();
    }
  }

}
