import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireAuthModule} from 'angularfire2/auth';

import { AppComponent } from './app.component';
import {AuthModule} from './auth/auth.module';
import {CoreModule} from './core/core.module';
import {AppRoutingModule} from './app-routing.module';
import {NotFoundModule} from './shared/not-found/not-found.module';
import {AuthGuard} from './shared/auth.guard';

import 'rxjs/add/observable/fromEvent';
// import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/throttleTime';
// import 'rxjs/add/operator/take';

// мимо
import {MyLittleDatabase} from './my-little-database.service';

import {NotificationModule} from './shared/notification/notification.module';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {AngularFireModule} from 'angularfire2';
import {firebaseConfig} from '../environments/firebase.config';
import {AngularFirestoreModule} from 'angularfire2/firestore';



@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireAuthModule,


    AngularFireModule.initializeApp(firebaseConfig),
    // enablePersistence() - сохраняет изменения в офлайне - как выходишь в онлайн - отправляет их
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,


    // CoreModule,  // lazyloading
    // порядок важен - ЖЭЭЭСТЬ!!

    AuthModule,
    AppRoutingModule,


    NotFoundModule,

    NotificationModule,
  ],
  providers: [MyLittleDatabase, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
