import {AfterViewInit, Component, ElementRef, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.css']
})
export class CoreComponent implements AfterViewInit {

  // stateNum = 0;

  @ViewChild('backgroundsList') backList: ElementRef;
  arrDivs = [];
  parentNode = null;
  backListEl = null;

  constructor(private renderer: Renderer2) { }

  ngAfterViewInit() {
    this.backListEl = this.backList.nativeElement;

    // делаем это чтобы получить parentNode
    const div = this.renderer.createElement('div');
    this.renderer.addClass(div, 'layout__back');
    this.renderer.appendChild(this.backListEl, div);
    this.arrDivs.push(div);

    this.parentNode = this.renderer.parentNode(div);
  }

  changeBackground(colorCode: number) {
    let newColor = 'circle-title';  // default color

    switch (colorCode) {
      case 0:
        newColor = 'circle-title';
        break;
      case 1:
        newColor = 'circle-first';
        break;
      case 2:
        newColor = 'circle-second';
        break;
      case 3:
        newColor = 'circle-third';
        break;
    }

    const newDivBG = this.renderer.createElement('div');
    this.renderer.appendChild(this.backListEl, newDivBG);
    this.renderer.addClass(newDivBG, 'layout__back');
    this.arrDivs.push(newDivBG);

    this.renderer.addClass(newDivBG, newColor);

    if (this.arrDivs.length > 10) {
      const firstDivBG = this.arrDivs.shift();
      this.renderer.removeChild(this.parentNode, firstDivBG);
    }

    // мигание цветов
    // if (this.stateNum === this.arrDivs.length) { this.stateNum = 0; }
    // this.stateNum += 1;
    // if (this.stateNum % 2 === 0) {
    //   this.renderer.addClass(newDivBG, 'circle-green');
    // } else if (this.stateNum % 3 === 0) {
    //   this.renderer.addClass(newDivBG, 'circle-red');
    // } else {
    //   this.renderer.addClass(newDivBG, 'circle-yellow');
    // }
    // мигание цветов

  }
}
