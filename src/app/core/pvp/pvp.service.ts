import { Injectable } from '@angular/core';
import {WebSocketService} from './web-socket.service';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {TicketService} from '../practice/ticket/ticket.service';
import {Subscription} from 'rxjs/Subscription';
import {NotifyService} from '../../shared/notification/notify.service';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class PvpService {

  userJoined$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  join$: Subject<any>;
  // joinResult$: Subject<any>;

  sendYourResult$: Subject<any>;







  //  NEW REALISATION
  // subs: Subscription[];
  subOnUserGetAnswer$: Subscription;

  requestToAddToRoom$: Subject<any>;

  responceToStartGame$: Subscription;
  responceToEndGame$: Subscription;

  yourEmail: string = null;

  sendResults$: Subject<any>;
  recieveResults$: Subscription;

  joinedToRoom$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  joinedToGame$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    private authService: AuthService,
    private notifyService: NotifyService,
    private ticketService: TicketService,
    private webSocketService: WebSocketService) {

    //  NEW REALISATION
      // подключение к комнате
    this.requestToAddToRoom$ = this.webSocketService.createWebSocketSubject('addingToRoom');
    this.requestToAddToRoom$.subscribe(value => {
      console.log('requestToAddToRoom$', value);

      // true-false
      this.joinedToRoom$.next(value);
    });

    // отслеживаем начало игры
    this.responceToStartGame$ = this.webSocketService.createWebSocketSubject('startGame')
      .subscribe(signalToStartGame => {
        console.log('responceToStartGame$', signalToStartGame);

        this.joinedToGame$.next(signalToStartGame);
        this.yourEmail = this.authService.getUserEmail();
      });

    // нужно написать интерфейс на получаемое сообщение
    this.responceToEndGame$ = this.webSocketService.createWebSocketSubject('endGame')
      .subscribe(gameResult => {
        console.log('responceToEndGame$ - gameResult', gameResult);

        const notifyType = gameResult['gameResult'] ? 'success' : 'warning';
        this.notifyService.generateNotification(gameResult['message'], notifyType);

        this.resetGame();
        // this.addREsults();
      });

    // обмен результатами с оппонентом
    this.sendResults$ = this.webSocketService.createWebSocketSubject('sendResultsPvp');
    this.recieveResults$ = this.webSocketService.createWebSocketSubject('recieveResultsPvp')
      .subscribe(value => {
        console.log('Ресиеве Резалтс ПВП', value);

      });



    //  подписать нужно, чтобы смотреть когда юзер дает ответ
    this.subOnUserGetAnswer$ = this.ticketService.ticketResults$
      .subscribe(value => {

        this.sendResultToOpponent(value);
        });

  //  NEW REALISATION

  }


  //  --- NEW REALISATION
  resetGame() {
    this.joinedToRoom$.next(false);
    this.joinedToGame$.next(false);
  }

  sendResultToOpponent(array: boolean[]) {
    this.sendResults$.next(array);
  }
  //  --- NEW REALISATION


  join(email: string) {
    if (!this.userJoined$.getValue()) {
      this.join$.next(email);
    }
  }

  // sendResult(array: boolean[]) {
  //   // тут ожидаю увидеть массив
  //   // const res = this.ticketService.ticketResults$.getValue();
  //   const res = array;
  //   let cntOfRightAnswer = 0;
  //   let cntOfNull = 0;
  //
  //   res.forEach(answer => {
  //     if (answer === true) {
  //       cntOfRightAnswer += 1;
  //     }
  //     if (answer === null) {
  //       cntOfNull += 1;
  //     }
  //   });
  // //  send
  //   const resToSend = {
  //     cntOfRight: cntOfRightAnswer,
  //     cntOfNull: cntOfNull,
  //     total: res.length
  //   };
  //   console.log('RES TO SEND', resToSend);
  //   this.sendYourResult$.next(resToSend);
  // }

}
