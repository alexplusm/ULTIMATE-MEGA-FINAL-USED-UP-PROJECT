import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import * as io from 'socket.io-client';

@Injectable()
export class WebSocketService {

  // Our socket connection
  private socket;

  // configs
  private port = '3331';
  private connectionUrl = `http://localhost:${this.port}`;

  constructor() {
    this.socket = io(this.connectionUrl);
  }

  connectToSocketByEvent(event: string): Subject<MessageEvent> {

    const observable = new Observable(_observer => {
      this.socket.on(`${event}`, data => {
        console.log(`-!-!- RECEIVE (${event})`, data);
        _observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

    const observer = {
      next: (data: Object) => {
        this.socket.emit(`${event}`, data);
        console.log(`-!-!- SEND (${event})`, data);
      }
    };
    return Subject.create(observer, observable);
  }

  createWebSocketSubject(eventName: string): Subject<any> {
    return <Subject<any>>this
      .connectToSocketByEvent(eventName)
      .map((response: any): any => {
        return response;
      });
  }
}
