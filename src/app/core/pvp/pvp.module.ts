import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PvpComponent } from './pvp.component';
import {ChatModule} from './chat/chat.module';
import {PvpService} from './pvp.service';
import {TicketModule} from '../practice/ticket/ticket.module';


@NgModule({
  imports: [
    CommonModule,

    ChatModule,
    TicketModule
  ],
  declarations: [PvpComponent],
  exports: [PvpComponent],
  providers: [PvpService]
})
export class PvpModule { }
