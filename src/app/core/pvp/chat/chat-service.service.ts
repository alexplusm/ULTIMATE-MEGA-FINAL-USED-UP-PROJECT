import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import {IMessageRecieve} from '../../model/i-message-recieve';
import {IMessageSend} from '../../model/i-message-send';
import {WebSocketService} from '../web-socket.service';
import {NotifyService} from '../../../shared/notification/notify.service';
import {Router} from '@angular/router';

@Injectable()
export class ChatService {

  // массив с сообщения из чатика
  messeges$: BehaviorSubject<IMessageRecieve[]> = new BehaviorSubject<IMessageRecieve[]>(null);

  sendAndRecieveMSG$: Subject<any>;

  constructor(
    private router: Router,
    private notyfyService: NotifyService,
    private webSocketService: WebSocketService) {

    // autoscroll test

        // const arr = [];
        // for (let i = 0; i < 2; i++) {
        //   arr.push(`alex hello ${i}`);
        // }
        // this.messeges$.next(arr);

    // autoscroll test


    this.sendAndRecieveMSG$ = this.webSocketService.createWebSocketSubject('sendAndReсieveMSG');

    this.sendAndRecieveMSG$.subscribe((message: IMessageRecieve) => {
      console.log('--- new message --- ', message);

      this.addMessageToChat(message);
    });
  }

  getMessages$() {
    return this.messeges$.asObservable();
  }

  addMessageToChat(message: IMessageRecieve) {
    console.log('addMessageToChat', message);

    let messageBuffer = this.messeges$.getValue();
    if (messageBuffer === null) { messageBuffer = []; }
    messageBuffer.push(message);
    this.messeges$.next(messageBuffer);
  }

  clearMessages() {
    this.messeges$.next(null);
  }

  sendMessage(message: IMessageSend) {
    this.sendAndRecieveMSG$.next(message);
  }
}
