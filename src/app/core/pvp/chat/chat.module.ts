import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ChatComponent} from './chat.component';
import {ChatService} from './chat-service.service';
import {WebSocketService} from '../web-socket.service';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [ChatComponent],
  exports: [ChatComponent],
  providers: [ChatService, WebSocketService]
})
export class ChatModule { }
