import { Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ChatService} from './chat-service.service';
import {IMessageSend} from '../../model/i-message-send';
import {AuthService} from '../../../auth/auth.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {IMessageRecieve} from '../../model/i-message-recieve';
import {PvpService} from '../pvp.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit, OnDestroy {

  isOpen = false;

  subs: Subscription[] = [];

  form: FormGroup;
  messages$: Observable<IMessageRecieve[]>;

  messageToSend: IMessageSend = {from: '', text: ''};

  $ev: Subscription;
  $flag: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  @ViewChild('chatWindow') chatWindow: ElementRef;
  // for autoscroll
  @ViewChild('chatBody') chatBody: ElementRef;

  constructor(
    private pvpService: PvpService,
    private chatService: ChatService,
    private authService: AuthService) { }

  ngOnInit() {
    this.subs.push(this.authService.getUser$().subscribe(user => {
      // записываем владельца сообщения
      this.messageToSend.from = (user !== null) ? user.email : 'anon' ;
    }));

    this.form = new FormGroup({
      inputMessage: new FormControl(null, Validators.maxLength(20))
    });

    this.messages$ = this.chatService.getMessages$();
  }

  onmousedown() {
    const el = this.chatWindow.nativeElement;
    this.$flag.next(true);

    this.$ev = Observable.fromEvent(document, 'mousemove')
      .filter(() => this.$flag.value === true)
      .throttleTime(25)
      .subscribe((move: MouseEvent) => {

        el.style.left = `${move.pageX - 160}px`;
        el.style.top = `${move.pageY - 20}px`;
    });
  }

  onmouseup() {
    this.$flag.next(false);
  }


  onSend() {

    // проверить что пользователь уже подгрузился!

    if (this.form.valid) {
      // достаем текст из формы

      this.messageToSend.text = this.form.controls.inputMessage.value;
      this.chatService.sendMessage(this.messageToSend);

      this.form.reset();
    }
  }

  getErrors(errors: any) {
    if (errors['maxlength']) {
      return 'покороче плес';
    }
    return;
  }

  scrollToBottom() {
    if (this.chatBody) {
      const el = this.chatBody.nativeElement;

      if (el.clientHeight <= el.scrollHeight) {
        el.scrollTop = el.scrollHeight;
      }
    }
  }

  closeChat() {
    this.isOpen = false;
  }

  openChat() {
    this.isOpen = true;
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
