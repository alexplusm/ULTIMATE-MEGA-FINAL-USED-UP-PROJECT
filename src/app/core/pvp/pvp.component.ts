import { Component, OnInit } from '@angular/core';
import {PvpService} from './pvp.service';
import {AuthService} from '../../auth/auth.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-pvp',
  templateUrl: './pvp.component.html',
  styleUrls: ['./pvp.component.css']
})
export class PvpComponent implements OnInit {
  userEmail: string;

  constructor(private pvpService: PvpService, private authService: AuthService) { }

  ngOnInit() {

    this.authService.getUser$().subscribe(user => {
      // записываем владельца сообщения
      if (user) {
        this.userEmail = user.email;
      }
    });
  }

  startGame() {

    this.pvpService.sendYourResult$.next(true);

    this.pvpService.join(this.userEmail);
  }

  addToRoom() {
    this.pvpService.requestToAddToRoom$.next(true);
  }

}
