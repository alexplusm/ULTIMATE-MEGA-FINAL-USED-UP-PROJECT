import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/count';

import {ITicket} from '../model/i-ticket';

// на всякий случай + для заполнения
// import {ticketProdDB} from '../../../../utils/tickets-prod-db';

import { AngularFireStorage } from 'angularfire2/storage';
import {IQuestion} from '../model/i-question';



interface First {
  text: string;
  name: string;
}
// создать промежуточный интерфейс для доставания данных

@Injectable()
export class RetrieveTicketsService {

  // ticket: AngularFirestoreCollection<Ticket>
  // ticketsCollectionRef: AngularFirestoreCollection;

  ticketsCollectionRef: AngularFirestoreCollection<any>;
  forTicketsCnt;

  constructor(
    private afs: AngularFirestore,
    private fireStorage: AngularFireStorage
  ) {

    this.retrieveTickets();
  }



  // для отображения количества билетов (плиточки с номерами)
  retrieveTickets() {
    this.ticketsCollectionRef = this.afs.collection('tickets', ref => ref);
    this.forTicketsCnt = this.ticketsCollectionRef.valueChanges();
  }

  // альтернативное название функции : МНОГАБУКАФ()
  //
  // использование :
  // this.subOnTicket = this.retrieveTickets.retrieveTicketByTicketNumber$(ticketNum)
  //   .subscribe(ticket => this.ticket = ticket);
  //
  retrieveTicketByTicketNumber$(ticketNumber): Observable<ITicket> {
    const ticketCollectionRef = this.afs.collection(`tickets/${ticketNumber}/questions`, ref => ref);
    return ticketCollectionRef.valueChanges().map(data => {
      // заполняю этот ticket и в конце его верну
      const ticket: ITicket = {
        ticketNumber: null,
        questions: []
      };

      // console.log('---init data', data);

      data.forEach(question => {
        const answers: [string] = [''];
        answers.pop();

        for (const key in question) {
          if (/answer\d/.test(key)) {
            // заполняем массив answers ( фильтруя null )
            if (question[`${key}`]) {
              answers.push(question[`${key}`]);
            }
          }
        }

        const _question: IQuestion = {
          questionNumber: question['questionNumber'],
          questText: question['questText'],

          image: null,
          answers: answers,
          ticketNumber: question['ticketNumber'],
          trueAnswer: question['trueAnswer'],
        };

        // ( заменяю названия картинок урлами на них)
        this.downloadImageByTicketNumAndImgName(question['ticketNumber'], question['image'])
            .subscribe(url => _question.image = url);
        // ( заменяю названия картинок урлами на них)

        if (ticket.ticketNumber === null) {
          ticket.ticketNumber = question['ticketNumber'];
        }

        ticket.questions.push(_question);
      });

      return ticket;
    });
  }


  downloadImageByTicketNumAndImgName(ticketNumber: number, imgName: string) {
    const imageRef = this.fireStorage.ref(`images/${ticketNumber}/${imgName}.jpg`);
                                                  // ref('images/1/pdd-01-03.jpg');
    return imageRef.getDownloadURL();
  }






  /*
  *   Для заболнения БД
  *
  *   Добавить в kонструктор:
  *
  // this.addFullTicket(ticketProdDB.ticket1);
  // this.addFullTicket(ticketProdDB.ticket2);
  // this.addFullTicket(ticketProdDB.ticket3);
   */

  addFullTicket(ticket: ITicket) {
    const ticketCollectionRef = this.afs.collection(`tickets/${ticket.ticketNumber}/questions`, ref => ref);

    // ticketCollectionRef.doc(id).set({name: 'name', text: 'text'});

    ticket.questions.forEach((question) => {
      ticketCollectionRef.doc(`${question.questionNumber}`).set({
        ticketNumber: question.ticketNumber,
        questionNumber: question.questionNumber,
        questText: question.questText,
        trueAnswer: question.trueAnswer,
        image: question.image,
        answer1: question.answers[0],
        answer2: question.answers[1],
        answer3: question.answers[2] ? question.answers[2] : null,
        answer4: question.answers[3] ? question.answers[3] : null,
        answer5: question.answers[4] ? question.answers[4] : null
        }
      );
    });
  }
}
