import { Component, OnInit } from '@angular/core';

import {AuthService} from '../../../auth/auth.service';
import {NotifyService} from '../../../shared/notification/notify.service';
import {Observable} from 'rxjs/Observable';
import {IUser} from '../../model/i-user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userEmail$: Observable<string>;

  constructor(
    private authService: AuthService,
    private notifyService: NotifyService
  ) {}

  ngOnInit() {
    this.userEmail$ = this.authService.getUserEmail$();
  }

  logout() {
    this.authService.logout();
    this.notifyService.generateNotification(
      'Досвидули',
      'success'
    );
  }

}
