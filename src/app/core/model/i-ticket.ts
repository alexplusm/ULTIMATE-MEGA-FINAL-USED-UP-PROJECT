import {IQuestion} from './i-question';

export interface ITicket {
  ticketNumber: number;
  questions: IQuestion[];
}
