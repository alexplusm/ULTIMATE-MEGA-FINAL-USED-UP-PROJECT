export interface IMessageRecieve {
  from: string;
  text: string;
  createdAt: string;
}
