export interface IQuestion {
  ticketNumber: number;
  questionNumber: number;

  image: string;
  questText: string;
  answers: [string];
  trueAnswer: string;
}
