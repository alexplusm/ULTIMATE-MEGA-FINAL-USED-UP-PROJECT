import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {CoreComponent} from './core.component';
import {PracticalSectionComponent} from './practice/practical-section.component';
import {TheoryComponent} from './theory/theory.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PvpComponent} from './pvp/pvp.component';
import {TicketComponent} from './practice/ticket/ticket.component';

import {AuthGuard} from '../shared/auth.guard';


const routes: Routes = [
  {path: '', component: CoreComponent, canActivate: [AuthGuard],  children: [
      {path: 'practice', children: [
          {path: ':ticketNumber', component: TicketComponent },
          {path: '', component: PracticalSectionComponent },
        ]},
      {path: 'theory', component: TheoryComponent },
      {path: 'dashboard', component: DashboardComponent},
      {path: 'pvp', component: PvpComponent},
    ]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
