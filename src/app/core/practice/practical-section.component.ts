import { Component, OnInit } from '@angular/core';
import {RetrieveTicketsService} from '../shared/retrieve-tickets.service';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';


@Component({
  selector: 'app-practical-section',
  templateUrl: './practical-section.component.html',
  styleUrls: ['./practical-section.component.css']
})
export class PracticalSectionComponent implements OnInit {

  tickets$: Observable<any>;

  constructor(
    private retrieveTicketsService: RetrieveTicketsService,
    private router: Router
  ) {}

  ngOnInit() {
    // получаем все доступные билеты в виде { ticketNumber: 1 }, ...
    this.tickets$ = this.retrieveTicketsService.forTicketsCnt;
  }

  getTicketNumber(ticketNumber: number) {
    this.router.navigate(['core', 'practice', `${ticketNumber}`]);
    // тут можно начать процесс загрузки ???
  }
}
