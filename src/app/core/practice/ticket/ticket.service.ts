import { Injectable } from '@angular/core';
import {MyLittleDatabase} from '../../../my-little-database.service';
import {ITicket} from '../../model/i-ticket';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()
export class TicketService {
  // храню информацию о выбранном билете
  // ticket: ITicket;
  // ticketLength: number;

  // текущий билет
  // ticket$: BehaviorSubject<ITicket>;


  // инициализируем новую длиннуу
  ticketLength$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  // массивчик с результатами, который идет в прогресс-бар
  ticketResults$: BehaviorSubject<boolean[]> = new BehaviorSubject<boolean[]>(new Array(0));

  // для отслеживания такущего вопроса
  currentQuestion$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  // отслеживаем нажатия 1, 2, 3, 4
  digitKeyPress$: Observable<number>;

  constructor(private myLittleDataBase: MyLittleDatabase) {

    this.setUpNewTicket(0);

    // возвращаем поток цифр, на которые нажимает пользователь
    this.digitKeyPress$ = Observable.fromEvent(document, 'keydown')
      .filter(event => (49 <= event['keyCode'] && event['keyCode'] <= 52))
      .map(event => Number(event['key']));

  }

  // буду сетапить данные в ngOnInit() - ticket.component
  setUpNewTicket(ticketLength: number) {
    this.currentQuestion$.next(0);

    const arr = new Array(ticketLength).fill(null);
    this.ticketResults$.next(arr);

    this.ticketLength$.next(ticketLength);
    // this.ticketLength = ticketLength;
  }



  setTicketResults(res: boolean, questNum: number) {
    const a = this.ticketResults$.value;
    a[questNum - 1] = res;
    this.ticketResults$.next(a);
  }

  setCurrentQuestion(num: number) {
    this.currentQuestion$.next(num);
  }

  incCurrentQuestion() {
    const curQuest = this.currentQuestion$.value;
    if (curQuest < (this.ticketLength$.getValue() - 1)) {
      this.currentQuestion$.next(curQuest + 1);
    }
  }

  decCurrentQuestion() {
    const curQuest = this.currentQuestion$.value;
    if (curQuest > 0) {
      this.currentQuestion$.next(curQuest - 1);
    }
  }
}
