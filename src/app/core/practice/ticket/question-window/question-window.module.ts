import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionWindowComponent } from './question-window.component';
import { AnswersBlockModule } from '../answers-block/answers-block.module';

@NgModule({
  imports: [
    CommonModule,
    AnswersBlockModule
  ],
  declarations: [QuestionWindowComponent],
  exports: [QuestionWindowComponent],
})
export class QuestionWindowModule { }
