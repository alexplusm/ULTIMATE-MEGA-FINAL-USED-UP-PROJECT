import {Component, Input} from '@angular/core';

import {IQuestion} from '../../../model/i-question';
import {TicketService} from '../ticket.service';


@Component({
  selector: 'app-question-window',
  templateUrl: './question-window.component.html',
  styleUrls: ['./question-window.component.css'],
})
export class QuestionWindowComponent {

  @Input() question: IQuestion;

  constructor(private ticketService: TicketService) { }

  // юзер кликает на вариант ответа и мы знает насколько он умный
  onAnswerClick(userAnswer: boolean) {
    this.ticketService.setTicketResults(userAnswer, this.question.questionNumber);
  }
}
