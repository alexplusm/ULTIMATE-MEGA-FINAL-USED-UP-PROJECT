import {Component, Input, OnDestroy, OnInit} from '@angular/core';

import {TicketService} from '../ticket.service';

import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit, OnDestroy {
  @Input() countOfQuestions: number;

  currentQuestion: number;
  resultsMassive: boolean[];
  length: number;

  subscriptions: Subscription[] = [];

  // subOnLength: Subscription;
  // subOnResults: Subscription;

  constructor(private ticketService: TicketService) { }

  ngOnInit() {
    this.subscriptions.push(this.ticketService.ticketLength$.subscribe(l => this.length = l));
    this.subscriptions.push(this.ticketService.ticketResults$.subscribe(value => this.resultsMassive = value));
    this.subscriptions.push(this.ticketService.currentQuestion$.subscribe(q => this.currentQuestion = q));

    // this.subOnLength = this.ticketService.ticketLength$.subscribe(l => this.length = l);
    // this.subOnResults = this.ticketService.ticketResults$.subscribe(value => this.resultsMassive = value);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    // this.subOnLength.unsubscribe();
    // this.subOnResults.unsubscribe();
  }








  onClick (index: number) {
    this.ticketService.setCurrentQuestion(index);

  }
}
