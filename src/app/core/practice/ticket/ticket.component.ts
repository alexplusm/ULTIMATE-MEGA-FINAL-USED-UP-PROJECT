import {Component, Input, OnDestroy, OnInit} from '@angular/core';

import {ITicket} from '../../model/i-ticket';
import {TicketService} from './ticket.service';
import {IQuestion} from '../../model/i-question';

import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {ActivatedRoute} from '@angular/router';

import {RetrieveTicketsService} from '../../shared/retrieve-tickets.service';


import {MyLittleDatabase} from '../../../my-little-database.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit, OnDestroy {


  ticket: ITicket;
  questions: IQuestion[];

  subCurrentQuestion: Subscription;
  subArrows: Subscription;

  currentQuestion = 0;

  ticketLength: number;

  @Input() pvpMode = false;


  constructor(
    // вместо этого должен быть нормальный сервис с загрузкой билета
    // private myLittleDataBase: MyLittleDatabase,
    private retrieveTicketsService: RetrieveTicketsService,
    private ticketService: TicketService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    if (this.pvpMode) {
      this.loadFirstTicket();
    } else {
      this.route.params
        .switchMap( ({ticketNumber}) => {
          // c firebase
          this.retrieveTicketsService.retrieveTicketByTicketNumber$(ticketNumber)
            .subscribe(ticket => {
              console.log('---ticket', ticket);
              this.ticket = ticket;
              this.ticketLength = ticket.questions.length;
              this.questions = ticket.questions;

              this.ticketService.setUpNewTicket(ticket.questions.length);
            });

          return ticketNumber;
        })
        .subscribe();
    }




    this.subCurrentQuestion = this.ticketService.currentQuestion$.subscribe((curQuest) => {
      this.currentQuestion = curQuest;
    });

    // перещелкивание с помощью стрелочек
    this.subArrows = Observable.fromEvent(document, 'keydown')
      .filter(event => (event['key'] === 'ArrowRight' || event['key'] === 'ArrowLeft'))
      .map(event => event['key'])
      .subscribe(arrow => {
        if (arrow === 'ArrowRight') {
          this.nextQuestion();
        } else {
          this.previousQuestion();
        }
      });
  }

  nextQuestion() {
    this.ticketService.incCurrentQuestion();
  }

  previousQuestion() {
    this.ticketService.decCurrentQuestion();
  }


  // for pvp mode
  loadFirstTicket() {
    this.retrieveTicketsService.retrieveTicketByTicketNumber$(1)
      .subscribe(ticket => {
        console.log('---ticket', ticket);
        this.ticket = ticket;
        this.ticketLength = ticket.questions.length;
        this.questions = ticket.questions;

        this.ticketService.setUpNewTicket(ticket.questions.length);
      });
  }


  ngOnDestroy() {
    this.subArrows.unsubscribe();
    this.subCurrentQuestion.unsubscribe();
  }
}
