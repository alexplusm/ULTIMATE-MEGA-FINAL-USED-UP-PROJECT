import {
  AfterViewInit, Component,
  ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output,
  QueryList, ViewChildren,
} from '@angular/core';

import {TicketService} from '../ticket.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-answers-block',
  templateUrl: './answers-block.component.html',
  styleUrls: ['./answers-block.component.css']
})
export class AnswersBlockComponent implements AfterViewInit, OnInit, OnDestroy {
  buttonWithTrueAnswer: ElementRef = null;
  selectedAnswer: string = null;

  subOnDigitPress: Subscription;
  // subOnSecondDigitPress: Subscription = null;

  answerIsGiven = false;

  @Input() questionNumber: number;
  @Input() answers: string[];
  @Input() trueAnswer: string;
  @Output() answerClick = new EventEmitter<boolean>();

  @ViewChildren('button') buttons: QueryList<ElementRef>;

  constructor(private ticketService: TicketService) {

  }

  ngAfterViewInit() {
    this.buttonWithTrueAnswer = this.buttons.find(
      button => button.nativeElement.classList.contains('answers-block__t'));
  }

  ngOnInit() {
    // берем поток из сервиса
    this.subOnDigitPress = this.ticketService.digitKeyPress$
      .filter(userAnswer => userAnswer !== null)
      // чтобы не спамили все объекты компонетов - сравнивем номер этого компонента с тем, что активен
      .filter(() => this.questionNumber === this.ticketService.currentQuestion$.value)
      //  продолжаем, если пользователь еще не ответил
      .filter(() => !this.answerIsGiven)
      // идет поток из 1-2-3-4. Тут отсекаем лишние цифры (если в этом вопросе, например 3 ответа)
      .filter((userAnswer) => userAnswer <= this.answers.length)
      .subscribe(userAnswer => {
        this.onDigitPress(userAnswer);
    });
  }

  onClick(answer: string) {
    this.applyAnswer(answer);
  }

  onDigitPress(answerNumber: number) {
    const userAnswer = this.answers[answerNumber - 1];
    this.applyAnswer(userAnswer);
  }

  applyAnswer(answer: string) {
    this.answerIsGiven = true;
    this.selectedAnswer = answer;
    this.answerClick.emit(answer === this.trueAnswer);
  }


  ngOnDestroy() {
    this.subOnDigitPress.unsubscribe();
  }
}
