import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswersBlockComponent } from './answers-block.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AnswersBlockComponent],
  exports: [AnswersBlockComponent]
})
export class AnswersBlockModule { }
