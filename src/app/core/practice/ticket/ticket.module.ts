import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketComponent } from './ticket.component';
import {QuestionWindowModule} from './question-window/question-window.module';
import {TicketService} from './ticket.service';
import {ProgressBarModule} from './progress-bar/progress-bar.module';

@NgModule({
  imports: [
    CommonModule,
    QuestionWindowModule,
    ProgressBarModule
  ],
  declarations: [TicketComponent],
  exports: [TicketComponent],
  providers: [TicketService]
})
export class TicketModule { }
