import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PracticalSectionComponent } from './practical-section.component';
import {TicketModule} from './ticket/ticket.module';

@NgModule({
  imports: [
    CommonModule,
    TicketModule
  ],
  declarations: [PracticalSectionComponent],
  exports: [PracticalSectionComponent]
})
export class PracticalSectionModule { }
