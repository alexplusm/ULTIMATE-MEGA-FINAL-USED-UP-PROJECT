import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireStorageModule} from 'angularfire2/storage';
import { firebaseConfig } from '../../environments/firebase.config';

import { CoreComponent } from './core.component';
import {CoreRoutingModule} from './core-routing.module';
import {HeaderModule} from './shared/header/header.module';
import {PracticalSectionModule} from './practice/practical-section.module';
import {TheoryModule} from './theory/theory.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {PvpModule} from './pvp/pvp.module';
import {RetrieveTicketsService} from './shared/retrieve-tickets.service';




@NgModule({
  imports: [
    CommonModule,

    HeaderModule,
    PracticalSectionModule,
    DashboardModule,
    PvpModule,
    TheoryModule,


    CoreRoutingModule,

    AngularFireModule.initializeApp(firebaseConfig),
    // enablePersistence() - сохраняет изменения в офлайне - как выходишь в онлайн - отправляет их
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,


  ],
  declarations: [CoreComponent],
  exports: [CoreComponent],

  providers: [RetrieveTicketsService]
})
export class CoreModule { }
