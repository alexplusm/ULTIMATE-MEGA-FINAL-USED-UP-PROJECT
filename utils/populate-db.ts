// declare const process: any;
//
// import { database, initializeApp} from 'firebase';
// import {firebaseConfig} from '../src/environments/firebase.config';
// import {data1} from './tickets-db';
//
// initializeApp(firebaseConfig);
//
// console.log('database', database);   // тут написано про storage
//
//
// // асинхронный слушатель - рычаг с помощью которого работаем с бд
// const testRef = database().ref('test');
//
// // дергаем за все ручки(урлы/списки..) и и ресетим их ( и ждем пока все просимы пройдут )
// async function reset() {
//   return Promise.all([
//     testRef.set(null)
//   ]);
// }
//
//
// // пишем данные - можно проверить возможности новго JS'a - сокращенную запись ( потом )
// function writeTestData(iid: number, nname: string) {
//   testRef.set({
//     name: nname,
//     id: iid
//   });
// }
//
// const di = data1.someData[0]['id'];
// const anme = data1.someData[0]['name'];
//
// async function populate() {
//   // await reset();
//
//   writeTestData(di, anme);
//
//   process.exit();
// }
//
// populate();





// const ticketsRef = database().ref('tickets');

// const walletsRef = database().ref('wallets');
// const purchasesRef = database().ref('purchases');
// const purchasesPerWalletsRef = database().ref('purchasesPerWallets');
//
//
// async function reset() {
//   return Promise.all([
//     datesRef.set(null)
//   ]);
// }
//
//
//
// function writeDates(ide, name) {
//   console.log('ide', ide, 'name', name)
//   datesRef.set({
//     ide: ide,
//     name: name
//   });
// }
//
//
//
// // async function reset() {
// //   return Promise.all([
// //     purchasesRef.set(null),
// //     walletsRef.set(null),
// //     purchasesPerWalletsRef.set(null)
// //   ]);
// // }

//
//
// // async function addWallet(wallet) {
// //   const walletRef = await walletsRef.push({
// //     name: wallet.name,
// //     amount: wallet.amount
// //   });
// //
// //   const newRefs = await Promise.all(wallet.purchases.map(purchase => purchasesRef.push(purchase)));
// //   const purchasesKeys = newRefs.map(({key}) => key);
// //   const purchasesPerWalletRef = await purchasesPerWalletsRef.child(walletRef.key);
// //
// //   return Promise.all(
// //     purchasesKeys.map(key => purchasesPerWalletRef.child(key).set(true))
// //   );
// // }
//
//
// async function populate() {
//   // await reset();
//   // await Promise.all(data.tickets.map(addWallet));


//   process.exit();
// }
//
// populate();














declare const process: any;

import {database, initializeApp} from 'firebase';
import {firebaseConfig} from '../src/environments/firebase.config';
import {dataw} from './tickets-db';

initializeApp(firebaseConfig);

const purchasesRef = database().ref('purchases');
const walletsRef = database().ref('wallets');
const purchasesPerWalletsRef = database().ref('purchasesPerWallets');

async function reset() {
  return Promise.all([
    purchasesRef.set(null),
    walletsRef.set(null),
    purchasesPerWalletsRef.set(null)
  ]);
}

async function addWallet(wallet) {
  const walletRef = await walletsRef.push({
    name: wallet.name,
    amount: wallet.amount
  });

  const newRefs = await Promise.all(wallet.purchases.map(purchase => purchasesRef.push(purchase)));
  const purchasesKeys = newRefs.map(({key}) => key);
  const purchasesPerWalletRef = await purchasesPerWalletsRef.child(walletRef.key);

  return Promise.all(
    purchasesKeys.map(key => purchasesPerWalletRef.child(key).set(true))
  );
}


async function populate() {
  await reset();
  await Promise.all(dataw.wallets.map(addWallet));

  process.exit();
}

populate();

