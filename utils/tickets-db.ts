import {ITicket} from '../src/app/core/model/i-ticket';

export const data = {
  tickets: [
    {
      ticketNumber: 1,
      questions: [
        {
          ticketNumber: 1,
          questionNumber: 1,
          image: '/assets/2.jpg',
          questText: 'БИЛЕТ1-Вы намерены произвести разворот на перекрестке.' +
          ' Какие указатели поворота необходимо включить перед въездом на перекресток?',
          answers: [
            'Правого поворота',
            'Левого поворота',
            'Включать указатели поворота при необходимости',
          ],
          trueAnswer: 'Правого поворота'
        },
        {
          ticketNumber: 1,
          questionNumber: 2,
          image: '/assets/3.jpg',
          questText: 'БИЛЕТ1-На каком рисунке изображен автомобиль, водитель которого нарушает правила перевозки грузов?',
          answers: [
            'Только на Б',
            'Только на А',
            'На обоих',
          ],
          trueAnswer: 'Только на А'
        },
        {
          ticketNumber: 1,
          questionNumber: 3,
          image: '/assets/4.jpg',
          questText: 'БИЛЕТ1-Кому Вы должны уступить дорогу при повороте налево?',
          answers: [
            'Только грузовому автомобилю',
            'Только легковому автомобилю',
            'Обоим транспортным средствам',
          ],
          trueAnswer: 'Обоим транспортным средствам'
        }
      ]
    },
    {
      ticketNumber: 2,
      questions: [
        {
          ticketNumber: 2,
          questionNumber: 1,
          image: '/assets/2.jpg',
          questText: 'БИЛЕТ2-Вы намерены произвести разворот на перекрестке.' +
          ' Какие указатели поворота необходимо включить перед въездом на перекресток?',
          answers: [
            'Правого поворота',
            'Левого поворота',
            'Включать указатели поворота при необходимости',
          ],
          trueAnswer: 'Правого поворота'
        },
        {
          ticketNumber: 2,
          questionNumber: 2,
          image: '/assets/3.jpg',
          questText: 'БИЛЕТ2-На каком рисунке изображен автомобиль, водитель которого нарушает правила перевозки грузов?',
          answers: [
            'Только на Б',
            'Только на А',
            'На обоих',
          ],
          trueAnswer: 'Только на А'
        },
        {
          ticketNumber: 2,
          questionNumber: 3,
          image: '/assets/4.jpg',
          questText: 'БИЛЕТ2-Кому Вы должны уступить дорогу при повороте налево?',
          answers: [
            'Только грузовому автомобилю',
            'Только легковому автомобилю',
            'Обоим транспортным средствам',
          ],
          trueAnswer: 'Обоим транспортным средствам'
        }
      ]
    }
  ]
}

export const data1 = {
  someData: [
    {
      id: 1,
      name: 'name1'
    },
    {
      id: 2,
      name: 'name2'
    },
  ]
};










export const dataw = {
  wallets: [
    {
      name: 'Мой первый кошелек',
      amount: 100000,
      purchases: [
        {
          title: 'Проезд на метро',
          price: 1700,
          date: '2017-11-23'
        },
        {
          title: 'IPhone X 256gb',
          price: 91990,
          date: '2017-11-23'
        },
        {
          title: 'Лапша "Доширак"',
          price: 40,
          date: '2017-11-23'
        }
      ]
    },
    {
      name: 'Второй кошелек',
      amount: 50000,
      purchases: [
        {
          title: 'Проезд на такси',
          price: 300,
          date: '2017-11-23'
        },
        {
          title: 'Виски "Jack Daniel\'s"',
          price: 1800,
          date: '2017-11-23'
        },
        {
          title: 'Вафли "Куку руку"',
          price: 80,
          date: '2017-11-23'
        }
      ]
    },
    {
      name: 'Мой третий кошелек',
      amount: 100000,
      purchases: [{
        title: 'Вафли "Куку руку"',
        price: 80,
        date: '2017-11-23'
      }]
    }
  ]
};
